
function pairs(obj) {

    if(typeof obj === 'undefined')
    {
        return 'TypeError';
    }
    let allPairs = [];
    let pairCount = 0;

    for(let key in obj)
    {
        let eachPair = [];
        eachPair[0] = key;
        eachPair[1] = obj[key];
        allPairs[pairCount++] = eachPair;
    }

    return allPairs;
}

module.exports = pairs;