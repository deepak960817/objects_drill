
function values(obj) {
    
    if(typeof obj === 'undefined')
    {    
        return 'TypeError';
    }
    let allValues = [];
    let valueCount = 0;
        
    for(let key in obj)
    {
        allValues[valueCount++] = obj[key];
    }
    return allValues;
}

module.exports = values;
