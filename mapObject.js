
function mapObject(obj, cb) {

    let mappedData = [];
    let keyCount=0;
    for(let key in obj)
    {
        mappedData[keyCount++] = cb(obj[key]);
    }

    return mappedData;
}

module.exports = mapObject;