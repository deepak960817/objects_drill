
function defaults(obj, defaultProps) {

    let updatedObj = obj;

    for(let defaultPropsKey in defaultProps)
    {   
        let flag = false;
        for(let objKey in updatedObj)
        {
           if(defaultPropsKey === objKey)
           {
               flag = true;
           }
        }
        if(flag===false)
        {
            let newEntry = {};
            newEntry[defaultPropsKey] = defaultProps[defaultPropsKey];
            Object.assign(updatedObj,newEntry);
        }
    }

    return updatedObj;
}
module.exports = defaults;