const value = require('../values.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const obj = { foo: 'bar', baz: 42 };
const arrayLikeObj1 = { 0: 'a', 1: 'b', 2: 'c' };
const arrayLikeObj2 = { 100: 'a', 2: 'b', 7: 'c' };
const my_obj = Object.create({}, { getFoo: { value: function() { return this.foo; } } });
my_obj.foo = 'bar';

const result = value(testObject);

console.log(result);

//console.log(Object.values(testObject));     //To verify code with standard Object method.
