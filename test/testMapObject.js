const _ = require('lodash');
const mapObj = require('../mapObject.js');

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

function add30(value)
{
    return value + 30;
}

const result = mapObj(testObject1, add30);
console.log(result);

//console.log(_.map(testObject1, add30));    //To test and comapred code output with standard method output.
