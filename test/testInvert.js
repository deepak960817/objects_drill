const _ = require('lodash');
const invertObj = require('../invert.js');

const object = { Moe: "Moses", Larry: "Louis", Curly: "Jerome" };
const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const result = invertObj(object);
console.log(result);

//console.log(_.invert(object));        //To test and comapred code output with standard method output.