const _ = require('lodash');
const pair = require('../pairs.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
//const obj = { foo: 'bar', baz: 42 };
const obj = { 0: 'a', 1: 'b', 2: 'c' };
const anObj = { 100: 'a', 2: 'b', 7: 'c' };
const myObj = Object.create({}, { getFoo: { value() { return this.foo; } } });
myObj.foo = 'bar';



const result = pair(testObject);

console.log(result);

console.log(Object.entries(testObject));    //To verify code with standard Object method.