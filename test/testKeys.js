const key = require('../keys.js');

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const arr = ['a', 'b', 'c'];
const obj = { 0: 'a', 1: 'b', 2: 'c' };
const anObj = { 100: 'a', 2: 'b', 7: 'c' };
const myObj = Object.create({}, {
    getFoo: {
        value: function () { return this.foo; }
    }
});
myObj.foo = 1;

const result = key(testObject);

console.log(result);

//console.log(Object.keys(testObject));    //To verify code with standard Object method.
