
function keys(obj) {

	if(typeof obj === 'undefined')
    {    
		return 'TypeError';
	}
	let allKeys = [];
	let keyCount = 0;

	for(let key in obj)
	{
		allKeys[keyCount++] = key;
	}
	return allKeys;
}

module.exports = keys;
