
function invert(obj) {

    let invertedObj = {};

    for(let key in obj)
    {
        let objKey = key;
        let objValue = obj[key];
        invertedObj[objValue] = objKey;
    }

    return invertedObj;
}

module.exports = invert;